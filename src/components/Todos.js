import React, { useEffect } from 'react';
import Todoform from './TodoForm';
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import { markComplete , deleteTodo, getTodos} from '../store/action/todoAction';
const Todos = ({todos,markCompleteProp, deleteTodoProp, getTodosProp}) => {
    useEffect(()=>{
        getTodosProp()
    },[])
    return (
        <div className='todo-list'>
            <Todoform />
            <ul>
                {
                  todos.map(todo =>(
                      <li key={todo.id} className={todo.completed ? "completed" : ""}>
                          {todo.title}
                          <input type="checkbox" onChange={markCompleteProp.bind(this,todo.id)}/>
                          <button onClick={deleteTodoProp.bind(this,todo.id)}>Delete</button>
                      </li>
                  )) 
                }
            </ul>
        </div>
    );
}

Todos.prototype = {
    todos: PropTypes.array.isRequired,
    markComplete: PropTypes.func.isRequired,
    deleteTodo: PropTypes.func.isRequired,
    getTodos: PropTypes.func.isRequired
}

const mapStateTodoProps = state => ({
    todos: state.myTodos.todos
})

export default connect(mapStateTodoProps,{markCompleteProp: markComplete, deleteTodoProp: deleteTodo, getTodosProp:getTodos})(Todos)
