import React from 'react';
import PropTypes from 'prop-types'
import {connect} from 'react-redux'

const Navbar = ({todos}) => {
    return (
        <div className="navbar">
            <h1>My Redux Todos App</h1>
            <ul>
                {/* <li>Home</li>
                <li>about</li> */}
                <li>Total todos: {todos.length}</li>
            </ul>
        </div>
    );
}

Navbar.prototype = {
    todos: PropTypes.array.isRequired
}

const mapStateTodoProps = state => ({
    todos: state.myTodos.todos
})

export default connect(mapStateTodoProps,{})(Navbar)
