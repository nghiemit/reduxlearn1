import React,{useState} from 'react';
import { v4 as uuidv4 } from 'uuid';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import { addTodo } from '../store/action/todoAction';
const Todoform = ({addTodo}) => {
    const [title,setTitle] = useState('')
    const onTitleChange = e =>{
        setTitle(e.target.value)
    }
    const onFormSubmit = e =>{
        e.preventDefault()
        if(title !== ""){
            const newTodo = {
                id: uuidv4(),
                title,
                completed: false,
            }
            console.log(newTodo)
            addTodo(newTodo)
            setTitle('')
        }
    }
    return (
        <div>
            <form onSubmit={onFormSubmit}>
                <input type="text" name="title" onChange={onTitleChange}/> 
                <input type="submit" value="Add" style={{cursor:"pointer"}}/>
            </form>
        </div>
    );
}

Todoform.prototype = {
    addTodo: PropTypes.func.isRequired
}
const mapStateTodoProps = state => ({

})

export default connect(mapStateTodoProps,{addTodo})(Todoform)
