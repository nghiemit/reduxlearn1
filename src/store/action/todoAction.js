// export const markComplete = () =>{
//     const markCompleteAction = dispatch => {
//         dispatch({
//             type: "MARK_COMPLETE",
//             payload: 'my payload'
//         })
//     }
//     return markCompleteAction
// }
import axios from 'axios'
const url = "https://jsonplaceholder.typicode.com/todos?_limit=2"
export const getTodos = () => async dispatch => {
    const res = await axios.get(url)
    dispatch({
        type: "GET_TODOS",
        payload: res.data
    })
}

export const markComplete = id => dispatch => {
  dispatch({
    type: "MARK_COMPLETE",
    payload: id,
  });
};

export const addTodo = newTodo =>async dispatch =>{
    await axios.post(url,newTodo)
    dispatch({
        type:"ADD_TODO",
        payload:newTodo
    })
}

export const deleteTodo = id => async dispatch => {
    await axios.delete(`https://jsonplaceholder.typicode.com/todos/${id}`)
    dispatch({
        type:"DELETE_TODO",
        payload:id
    })
}