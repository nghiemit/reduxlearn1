import {createStore,applyMiddleware} from 'redux'
import thunk from 'redux-thunk'
import rootReducer from './reduces'
import {composeWithDevTools} from 'redux-devtools-extension'

const initState = {}

const middleware = [thunk]

const store = createStore(
    rootReducer,
    initState,
    applyMiddleware(...middleware)
)

export default store;