import {combineReducers} from 'redux';
import todoReducer from './todoReduce'

const rootReducer = combineReducers({
    myTodos: todoReducer
})

export default rootReducer